package it.unibo.oop.util;

public interface Matrix<E> extends BaseMatrix<E> {

    void set(int i, int j, E e);
    void setDiagonals(int d, int a, E e);
    void setAll(E e);
    void setAll(BiIntObjFunction<E, E> f);

    static <E> Matrix<E> empty(int rows, int columns) {
        return new ArrayMatrix<>(rows, columns);
    }

    static <E> Matrix<E> of(int rows, int columns, E defaultElement) {
        final Matrix<E> matrix = new ArrayMatrix<>(rows, columns);
        matrix.setAll(defaultElement);
        return matrix;
    }

    static <E> Matrix<E> fill(int rows, int columns, BiIntFunction<E> generator) {
        final Matrix<E> matrix = new ArrayMatrix<>(rows, columns);
        final BiIntObjFunction<E, E> setter = (i, j, e) -> generator.apply(i, j);
        matrix.setAll(setter);
        return matrix;
    }
}
